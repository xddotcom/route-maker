
function getElementById(element){
  if(element[0]=='#'){
	  element = element.substring(1)
  }
  return document.getElementById(element);
}

function changeTheme(button, toTheme) {
	debug("changeTheme", button, toTheme);
	var r = new RegExp( "ui-btn-(up|down)-(.)" )
	var c = jQuery(button).attr('class');
	var f = r.exec(c);
	if(!f || f.length < 3){
		return;
	}
	var p = f[1];
	var t = f[2];
	jQuery(button).removeClass("ui-btn-"+p+"-"+t).addClass("ui-btn-"+p+"-"+toTheme);
}

function requestFullscreen(){
	var docElm = document.documentElement;
	if (docElm.requestFullscreen) {
		docElm.requestFullscreen();
	}
	else if (docElm.mozRequestFullScreen) {
		docElm.mozRequestFullScreen();
	}
	else if (docElm.webkitRequestFullScreen) {
		docElm.webkitRequestFullScreen();
	}
}


function addCell( table, texts, options ){

	var rowClass = options.rowClass;
	var colClass = options.colClass;
	var object = options.object;
	var functionPointer = options.functionPointer;
	var style = options.style;

	var row = document.createElement("TR");
	row.className = rowClass;
	var cell = document.createElement("TD");
	cell.className = colClass;

	for (var text in texts) {
		if(cell.hasChildNodes()){
			cell.appendChild(document.createElement("BR"));
		}
		var textnode = document.createTextNode(texts[text]);
		cell.appendChild(textnode);
	}
	row.appendChild(cell);
	table.appendChild(row);

	row.style.cursor = 'pointer';
	row.object = object;
	row.onclick = functionPointer;

	for(var key in style){
		cell.style[key] = style[key];
	}

	return cell;
}

function removeAll(element){
  if(element[0]=='#'){
	  element = element.substring(1)
  }
  var cell = document.getElementById(element);
  if ( cell !== null && cell.hasChildNodes() ){
      while ( cell.childNodes.length >= 1 ){
          cell.removeChild( cell.firstChild );
      }
  } else if ( cell !== null) {
	  debug("removeAll: Cell "+element+" is empty");
  } else {
	  debug("*** removeAll: Cannot find "+element);
  }
}

function getSelectedInRadios(radiolist){
	var l = jQuery(radiolist).find(":radio");
	debug(l.length);
	for(var i = 0 ; i < l.length; i++){
		if( jQuery(l[i]).attr("checked") ){
			return jQuery(l[i]).val();
		}
	}
}
