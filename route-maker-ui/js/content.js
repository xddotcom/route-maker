var view = null;
var controller = null;
var wideWidth = 700;
var mainIsShowing = false;
var remapNeeded = true;

var doInitOnce = true;
function initialize() {
	if (doInitOnce) {
		doInitOnce = false;
	} else {
		return;
	}

	initFields();
	initModel();
	initAfterResize();
}

var doinitFieldsOnce = true;
function initFields() {
	if (doinitFieldsOnce) {
		doinitFieldsOnce = false;
	} else {
		return;
	}

	var startDate = new Date();
	if (startDate.getHours() > 21) {
		startDate.setDate(startDate.getDate() + 1);
		startDate.setHours(8);
		startDate.setMinutes(0);
	} else {
		startDate.setHours(startDate.getHours() + 1);
	}
	startDate.setSeconds(0);

	debug("Main::initFields");
	$('#departureTime').scroller({
		preset : 'datetime',
		minDate : new Date(),
		theme : 'default',
		display : 'modal',
		mode : 'scroller',
		dateOrder : 'ddM',
		dateFormat : 'dd MM yyyy',
		timeFormat : 'HH:ii',
		timeWheels : 'HHii'
	}).scroller("setDate", startDate, true);

	$(".auto_inactivate").click(function(event) {
		var _this = this;
		setTimeout(function() {
			$(_this).removeClass("ui-btn-active");
			$(_this).addClass("ui-btn-inactive");
		}, 100);
	});

	$("#resetButton").click(function(event) {
		remap();
	});

}
function initModel() {
	debug("Main::initModel");

	var model = new Model(new Config());
	view = new View(model);

	controller = new Controller(view, model, {
		continuous : [ "#origin", "#destination", "#departureTime" ],
		resetButtons : [ "#resetOptions", "#resetOptions2" ],
		backButtons : [ "#optionsBackButton" ],
		origin : "#origin",
		destination : "#destination",
		departureTime : "#departureTime",
		nbMaxChanges : "#nbMaxChanges",
		maxWalkInM : "#maxWalkInM",
		optims : {
			"CHANGES" : "#optimChanges",
			"WALK" : "#optimWalk"
		}
	});
	controller.getStatus();
	controller.loadNetworks();

	initMap(controller);
}

function initMap(controller) {
	var myOptions = {
		zoom : 5,
		center : new google.maps.LatLng(0, 0),
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

	if (controller) {
		controller.setMap(map);
	}

	return map;
}

function setViewOptions() {
	debug("Main::setViewOptions");
	if (!view) {
		return;
	}

	var wide = ($(document).width() > wideWidth);
	var options = {
		leftPanel : true,
		oriDesFields : {
			O : "#origin",
			D : "#destination"
		},
		displayLimits : null,
		displayItins : "#resultsList",
		displayItinsFmt : "<li>%s</li>",
		displayDetails : "#detailsList",
		displayContext : "#context_bar",
		displayStatus : (wide ? "#status_bar" : "#context_bar"),
		displayInfo : "#info_panel",
		triggerDetails : "#detailsButton",
		triggerResults : "#resultsButton",
		boundsMargin : 0,
		wide : wide,
		fixedOrigin : "#fixedOrigin",
		sizeOfLine : "#sizeOfLine",
		triggerZoom : "#zoomButton",
		lineNoOptions : (wide ? "&nbsp;<br/>Click on map to select origin or destination"
				: "Please set parameters"),
	}
	view.setOptions(options);
}

function initAfterResize() {

	var width = $(document).width();
	var wide = (width > wideWidth);

	debug("Main::initAfterResize(wide=" + wide + ")");
	if (wide) {
		$("#optionsButton").find(".ui-btn-text")
				.text("set search options");
		$("#detailsButton").find(".ui-btn-text").text("view details");
		$("#resultsButton").find(".ui-btn-text").text("view all results");

		$("#context_bar").css({
			"display" : "inline-block",
			"width" : "65%"
		});
		$("#status_bar").css({
			"display" : "inline-block",
			"width" : "25%"
		});
	} else {
		$("#optionsButton").find(".ui-btn-text").text("params");
		$("#detailsButton").find(".ui-btn-text").text("details");
		$("#resultsButton").find(".ui-btn-text").text("results");

		$("#context_bar").css({
			"display" : "inline-block",
			"width" : "100%"
		});
		$("#status_bar").css({
			"display" : "none"
		});
	}
	setViewOptions();
	remap();
}
function remap() {
	//initMap(controller);

	remapNeeded = !mainIsShowing;
	if (mainIsShowing) {
		debug("Main::remap()");

		debug($(document).width() + ", " + $(window).height());

		$("#map_canvas").width($(document).width());

		if (false && navigator.userAgent.match(/Android/i)) {
			debug("Android");
			$("#map_canvas").height(
					$(document).height()
							- $("div.ui-footer").outerHeight()
							- $("div.ui-header").outerHeight());
			window.scrollTo(0, 1);
		} else {
			debug("Standard");
			$("#map_canvas").height(
					$(window).height()
							- $("div.ui-footer").outerHeight()
							- $("div.ui-header").outerHeight());
		}
	}
}

$('#main').live('pageshow', function(event) {
	debug("Main#main::pageshow");
	mainIsShowing = true;
	$.mobile.loadPage("#options");
	$.mobile.loadPage("#results");
	$.mobile.loadPage("#details");
	$.mobile.loadPage("#dialog-choose");

	if (remapNeeded) {
		remap();
	}
});
$('#main').live('pagehide', function(event) {
	debug("Main#main::pagehide");
	mainIsShowing = false;
});

$(document).bind('orientationchange', function(event) {
	debug("Main::orientationchange");
	initAfterResize();
});
$(window).resize(function(event) {
	debug("Main::resize");
	initAfterResize();
});

$('#options').live('pageshow', function(event) {
	debug("Main#options::pageshow");
	/* just in case we refresh page when on this page !*/
	requestFullscreen();
	initFields();
});

$(document).ready(function() {
	debug("Document is ready");
	initialize();
});
