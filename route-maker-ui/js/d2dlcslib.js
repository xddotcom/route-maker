var __debug__ = gup("debug");

function debug(message){
	if(__debug__ && console.debug) {
		console.debug(message);
	}
}

function isset(v){
	return v !== undefined && v !== null;
}

function dateFormatAsTime(date){
	  if(!date){
		  return "";
	  }
	  var h = date.getHours();
	  if (h<10){
		   h = "0"+h;
	  }
	  var m = date.getMinutes();
	  if (m < 10){
		  m = "0"+m;
	  }
	  return h + ":" + m;
}

function dateFormatAsDate(date){
	  if(!date){
		  return "";
	  }
	  var y = date.getFullYear();
	  var m = date.getMonth()+1;
	  if (m<10){
		   m = "0"+m;
	  }
	  var d = date.getDate();
	  if (d < 10){
		  d = "0"+d;
	  }
	  return y + "-" + m + "-" + d;
}

function smartRound(dble){
	if(dble < 10){
		return dble;
	}
	var f = Math.pow(10, Math.floor(Math.log(dble)/Math.LN10)-1);
	return f * Math.round(dble/f);
}


/**
 *
 * @param time
 * @param hF
 * @param mF
 * @param sF
 * @returns
 */
function timeFormat(time, hF, mF, sF, hU, mU, sU){
	var hour = parseInt(time / 3600);
	var minute = parseInt((time % 3600) / 60);
	var second = parseInt(time % 60);

	var h = "";
	var m = "";
	var s = "";

	if(!hU){
		hU = "h";
	}
	if(!mU){
		mU = "'";
	}
	if(!sU){
		sU = '"';
	}

	var hu = "";
	var mu = "";
	var su = "";

	if(sF > 0){
		su = sU;
		second = parseInt(sF*Math.round(second/sF));
		if(second==60){
			second = 0;
			minute += 1;
		}
		if (second < 10 && mF > 0 ){
			second = "0"+second;
		}
		s = ":" + second;
	}
	if(mF > 0){
		minute = parseInt(mF*Math.round(minute/mF));
		if(minute == 60){
			minute = 0;
			hour += 1;
		}
		if (minute < 10 && hF > 0 ){
			minute = "0"+minute;
		}
		m = minute;
		if( s != "" ){
			/* we have seconds ; add minute as separator */
			mu = mU;
		}
	}
	if(hF > 0){
		hour = parseInt(hF*Math.round(hour/hF));
		if(hour > 0){
			h = hour;
			hu = hU;
		} else {
			/* we have no hours ; add minute unit */
			mu = mU;
		}
	}

	return h + hu + m + mu + s + su;
}

function parseDate( date ){
	   var reg = new RegExp("[ T:-]+", "g");
	   var values = date.split(reg);

	   if(values.length == 3){
		    return new Date( values[0], values[1], values[2]);
	   } else if(values.length == 5){
		    return new Date( values[0], values[1], values[2], values[3], values[4] );
	   } else if(values.length == 6){
		   return new Date( values[0], values[1], values[2], values[3], values[4], values[5] );
	   }
	   return null;
}

function to_map(str) {
	ret = {};
	if( str !== undefined && str !== null){
		a = str.split(";");
		for(index in a){
			b = a[index].split("=");
			ret[b[0]] = b[1];
		}
	}
	return ret;
}

function implode(array, glue, glue2) {
	if( arguments.length <= 1 ){
		glue = "";
	}
	if( arguments.length <= 2 ){
		glue2 = glue;
	}
	return [].join.apply(array, [glue]);
}

function gup( name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

String.prototype.trim = function(c,right,left) {
	r = new RegExp("^"+c+"+|"+c+"+$", "g");
    return String(this).replace(r, '');
};
String.prototype.ltrim = function(c,right,left) {
	r = new RegExp("^"+c+"+", "g");
    return String(this).replace(r, '');
};
