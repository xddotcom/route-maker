var Event = function (sender) {
    this._sender = sender;
    this._listeners = [];
};

Event.prototype = {
    attach : function (listener) {
        this._listeners.push(listener);
    },
    notify : function (args) {
        for (var i = 0; i < this._listeners.length; i++) {
            this._listeners[i](this._sender, args);
        }
    }
};

var Model = function (config) {
	this._limits      = new Event(this);
	this._itineraries = new Event(this);
	this._solutions   = new Event(this);
	this._address     = new Event(this);
	this._position    = new Event(this);
	this._directions  = new Event(this);
	this._status      = new Event(this);

	this.config = config;
	this.door2doorURL = this.config.door2doorURL ; // "http://localhost:8080/d2d/";
	this.localtransitURL = this.config.localtransitURL ; // "http://localhost:8080/lcs/";

	this._events = {
		limits :     this._limits,
		itineraries: this._itineraries,
		solutions:   this._solutions,
		address:     this._address,
		status:      this._status,
		directions:  this._directions,
		position:    this._position
	};

	this.maximumEstimatedDistance = 250; /* meters */
	this.speed = 0.6; /* meters per second */

	/* Geocoder */
	this._geocoder = new google.maps.Geocoder();

	/* Directions */
	this._directionService = new google.maps.DirectionsService();
	this._directionsCache = {};
};

Model.prototype = {
	call: function(async, url, data, callback){
		jQuery.ajax({
			url      : url,
			data     : data,
			async    : async,
			dataType : "json",
			success  : callback
		});
	},
	bind: function(event, listener) {
		this._events[event].attach(listener);
	},
	getStatus: function () {
		var url = this.localtransitURL + "status.html";
		var _event = this._status;
		this.call(true,
		          url,
		          {},
		          function(data) {_event.notify(data);}
		          );
	},
	loadNetworks: function (){
		var url = this.localtransitURL + "networks.html";
		var _event = this._limits;
		jQuery.getJSON(url, function(data) {
			points = data["points"];
			limits = data["limits"];
			bundle = [];
	  	  	for(var limitIdx in limits){
	  	  		var limit = limits[limitIdx];
	  	  		var sw = points[limit.sw];
	  	  		var ne = points[limit.ne];
	  	  	    bundle.push({sw:sw,
	  	  	    	         ne:ne,
	  	  	    	         shape:limit.shape,
	  	  	    	         name: limit.name,
	  	  	    	         content: limit.content});
	  	  	}
	  	    _event.notify(bundle);
		});
	},
	loadSolutions: function(origin, destination, date, time, nbChanges, maxwalk, optims) {
		if(!origin||!destination){
			return false;
		}
		if(maxwalk >= 5000 ){
			maxwalk = null;
		}
		var url = this.door2doorURL + "itinerary.html";
		var _event = this._solutions;
		this.call(false,
		          url,
		          { nbrecs   : 5
				   , segments : [{ origin      : origin.lat()+","+origin.lng()
                	             , destination : destination.lat()+","+destination.lng()
                	             , date        : date
                	             , time        : time
				                 }]
		          },
  				 function(data) {
  					 /* get server stats */
  					 var vars = data["vars"];
  					 var status = {};
  					 if(isset(vars)){
	  					 for(var key in vars){
	  						status[key] = vars[key].value;
	  					 }
	  					 if( isset(vars["error"]) ){
	  						 var errno = vars["errno"].value;
	  						 var error = vars["error"].value;
	  						 debug( "Error " + error + " (" + errno + ")");
	  					 }
  					 }
  					 /* get result */
  					 var solutions = data["solutions"];
  					 var bundle = [];
  					 for(var solutionIdx in solutions){
  						 var i = new Itinerary();
  						 i.buildFromSolution(solutionIdx, data);
  						 bundle.push(i);
  					 }
  					 _event.notify({itineraries:bundle, status:status});
  				 });
		return true;
	},
	loadItineraries: function(origin, destination, date, time, nbChanges, maxwalk, optims) {
		if(!origin||!destination){
			return false;
		}
		if(maxwalk >= 5000 ){
			maxwalk = null;
		}
		var url = this.localtransitURL + "itinerary.html";
		var _event = this._itineraries;
		jQuery.getJSON( url,
					   {
                         origin:      origin.lat()+","+origin.lng(),
		  				 destination: destination.lat()+","+destination.lng(),
		  				 date: date,
		  				 time: time,
		  				 maxchanges:  nbChanges,
		  				 maxwalk: maxwalk,
		  				 maxduration: 480,
		  				 optims: implode(optims,",")
		  				},
		  				 function(data) {
		  					 /* get server stats */
		  					 var vars = data["vars"];
		  					 var status = {};
		  					 if(isset(vars)){
			  					 for(var key in vars){
			  						status[key] = vars[key].value;
			  					 }
			  					 if( isset(vars["error"]) ){
			  						 var errno = vars["errno"].value;
			  						 var error = vars["error"].value;
			  						 debug( "Error " + error + " (" + errno + ")");
			  					 }
		  					 }
		  					 /* get result */
		  					 var itins = data["itins"];
		  					 var bundle = [];
		  					 for(var itinIdx in itins){
		  						var i = new Itinerary();
		  						i.buildFromItinerary(itinIdx, data);
		  						bundle.push(i);
		  					 }
		  					 _event.notify({itineraries:bundle, status:status});
		  				 });
		return true;
	},
	getAddress: function(point, arg) {
		debug("Model::getAddress: address="+point);
		var _event = this._address;
		this._geocoder.geocode({'latLng': point}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				_event.notify([{ point:   point,
				                 address: results[0].formatted_address,
				                 name:    results[1].formatted_address,
					             arg:     arg }]);
			} else {
				_event.notify([{
					point:   point,
					address: null,
					name:    null,
					arg:     arg
				}]);
			}
        });
	},
	getPosition: function(address, arg) {
		debug("Model::getPosition: address='"+address+"'");
		var _event = this._position;
		this._geocoder.geocode({address: address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var r = [];
				debug(results);
				for (var index in results){
					rr = {
						point:       results[index].geometry.location,
						address:     results[index].formatted_address,
						arg:         arg,
						postal_code: "",
						locality:    "",
					};
					for( var comp in results[index].address_components){
						if( jQuery.inArray( "route", results[index].address_components[comp].types ) >= 0 ){
							rr.name = results[index].address_components[comp].long_name;

						} else if( jQuery.inArray( "locality", results[index].address_components[comp].types ) >= 0 ){
							rr.locality += results[index].address_components[comp].long_name + " ";

						} else if( jQuery.inArray( "administrative_area_level_2", results[index].address_components[comp].types ) >= 0 ){
							rr.locality += results[index].address_components[comp].long_name + " ";

						} else if( jQuery.inArray( "postal_code", results[index].address_components[comp].types ) >= 0 ){
							rr.postal_code = results[index].address_components[comp].long_name;
						}
					}
					debug(rr);
					r.push(rr);
				}

				_event.notify(r);
			} else {
				_event.notify([{
					arg:     arg,
					name:    null,
					point:   null,
					address: null }]);
			}
        });
	},
	/**
	 *
	 * Triggers "directions" event
	 *
	 * @param p1 google.maps.LatLng
	 * @param p2 google.maps.LatLng
	 * @return array of google.maps.LatLng
	 */
	getDirections: function(point1, point2, arg) {
		debug("Model::getDirections: point1='"+point1+"', point2='"+point2+"'");
		var _event = this._directions;
		var _this = this;
		var key =  point1.lat()+":"+point1.lng()+"/"+point2.lat()+":"+point2.lng();

		var r = this.getDirectionsSync(point1, point2, arg);
		if(r){
			_event.notify(r);
			return;
		}

		var request = {
			origin: point1,
			destination: point2,
	        travelMode: google.maps.TravelMode.WALKING
		};
		this._directionService.route(request, function(response, status) {
			debug(response);
			if (status == google.maps.DirectionsStatus.OK) {
				var r = response.routes[0];
				_this._directionsCache[key] = r;
				_event.notify({ path : r.overview_path,
					            distance: r.legs[0].distance.value,
					            duration: r.legs[0].duration.value,
					            arg: arg});
		    }
		});
	},
	/**
	 *
	 * Triggers "directions" event
	 *
	 * @param p1 google.maps.LatLng
	 * @param p2 google.maps.LatLng
	 * @return array of google.maps.LatLng
	 */
	getDirectionsSync: function(point1, point2, arg) {
		debug("Model::getDirections: point1='"+point1+"', point2='"+point2+"'");

		var key =  point1.lat()+":"+point1.lng()+"/"+point2.lat()+":"+point2.lng();
		var direct = distHaversine(point1, point2);

		if( direct < this.maximumEstimatedDistance ){
			/* Estimation:
			 *    for short distances   d = direct
			 *    for long distances    d = sqrt(2)*direct
			 *
			 *    General formula
			 *             d = f * direct
			 *             f = 1 + (sqrt(2)-1) * (1-2^(-direct/period))
			 *        period = 100 m (6 period ~ 100%)
			 *
			 *          d(0) = direct
			 *        d(inf) = sqrt(2) * direct
			 */
			var period = 100;
			var f = 1 + (Math.sqrt(2)-1) * (1-Math.pow(2,-direct/period));
			var distance = parseInt(10 * Math.round(f * direct / 10));

			return { path: [point1, point2],
				     distance: distance,
                     duration: distance / this.speed,
				     arg: arg};
		}
		r = this._directionsCache[key];
		if( r ){
			return { path : r.overview_path,
	                 distance: r.legs[0].distance.value,
	                 duration: r.legs[0].duration.value,
	                 arg: arg};
		}
		return null;
	}
};


function Itinerary(){
	this.connexions = new Array();
	this.origin = null;
	this.destination = null;
	this.free_txt = null;
}
Itinerary.prototype = {
	buildConnexion: function(cnx, segments, points, carriers){
		var ori = points[cnx.ori];
		var des = points[cnx.des];

		if( this.origin === null ){
			this.origin = ori;
		}
		this.destination = des;
		var via = [];

		if(isset(segments)){
			for(var segIdx in cnx.segs){
				var seg = segments[cnx.segs[segIdx]];
				via.push( points[seg.des] );
			}
		}

		color = cnx.color;
		txtcolor = cnx.txtcolor;
		if(color == 0 || color === undefined ) {
			txtcolor = "black";
			color = "lightblue";
		} else {
			txtcolor = "#"+txtcolor;
			color = "#"+color;
		}

		var operator = null;
		if( isset(cnx.opr) ){
			operator = carriers[cnx.opr].name;
		}
		var cnxobject = { origin       : ori
				        , via          : via
				        , destination  : des
				        , departure    : parseDate(cnx.dep)
				        , arrival      : parseDate(cnx.arr)
				        , operator     : operator
				        , name         : cnx.name
				        , route        : cnx.rte
				        , reference    : cnx.ref
				        , color        : color
				        , type         : cnx.type
				        , txtcolor     : txtcolor
				        , alternatives : []
		};

		return cnxobject;
	},
	addSingleConnexion: function(cnx, segments, points, carriers){
		var cnxobject = this.buildConnexion(cnx, segments, points, carriers);
		this.connexions.push( cnxobject );
		return cnxobject;
	},
	addMultipleConnexion: function(cnx, segments, points, carriers){
		for(var segIdx in cnx.segs){
			var seg = segments[cnx.segs[segIdx]];
			var cnxobject = this.buildConnexion(seg, null, points, carriers);
			this.connexions.push( cnxobject );
		}
	},
	buildFromItinerary: function( itinIdx, data ){
	    points = data["points"];
	    segments = data["segments"];
	    connections = data["connections"];
	    itins = data["itins"];
	    carriers = data["carriers"];
	    var itin = itins[itinIdx];

	    this.free_txt = itin.ftxt;

	    for(var cnxPos in itin.cnxs){
	    	var cnxIdx = itin.cnxs[cnxPos];
	    	var cnx = connections[cnxIdx];
	    	this.addSingleConnexion(cnx, segments, points, carriers);
	    }
	},
	buildFromSolution: function( solutionIdx, data ){
	    points = data["points"];
	    segments = data["segments"];
	    connections = data["connections"];
	    itins = data["itins"];
	    solutions = data["solutions"];
	    carriers = data["carriers"];
	    topologies = data["topologies"];

	    var solution = solutions[solutionIdx];
	    var tmp_itineraries = [];

	    for(var itinPos in solution.itins){

	    	var itinIdx = solution.itins[itinPos];
	    	var itin = itins[itinIdx];
	    	var tmp_connexions_of_itinerary = [];
	    	for(var cnxPos in itin.cnxs){
		    	var cnxIdx = itin.cnxs[cnxPos];
		    	tmp_connexions_of_itinerary.push(cnxIdx);
	    	}

	    	var tmp_alternatives = [];
	    	for(var altItinPos in itin.alt ){
		    	var altIdx = itin.alt[altItinPos];
		    	var alt = itins[altIdx];
		    	var tmp_connexions_of_aternative = [];
		    	for(var cnxPos in alt.cnxs){
			    	var cnxIdx = alt.cnxs[cnxPos];
			    	tmp_connexions_of_aternative.push(cnxIdx);
		    	}
		    	tmp_alternatives.push(tmp_connexions_of_aternative);
	    	}

	    	tmp_itineraries.push( { connexions  : tmp_connexions_of_itinerary
                                  , alternatives: tmp_alternatives
		    	                  , current     : 0 });

	    }

	    for(var topoPos in solution.topos ){
    		var topoKey = solution.topos[topoPos];
    		var topo = topologies[topoKey];
    		for( var itemPos in topo.items ){
        		var item = topo.items[itemPos];

        		var c = tmp_itineraries[item].current;
        		//var cnxIdx;
        		/* get first alternative */
        		if( tmp_itineraries[item].alternatives.length > 0 ){
        			cnxIdx = tmp_itineraries[item].alternatives[0][c];
        		} else {
        			cnxIdx = tmp_itineraries[item].connexions[c];
        		}
    	    	//var cnx = connections[cnxIdx];
    	    	//var cnxobject = this.addMultipleConnexion(cnx, segments, points, carriers);

    	    	/*
    	    	for(var alt in tmp_itineraries[item].alternatives ){
    	    		var altcnxIdx = tmp_itineraries[item].alternatives[alt][c];
    	    		var altcnx = connections[altcnxIdx];
    	    		var altcnxobejct = this.addMultipleConnexion(altcnx, segments, points, carriers);
    	    		cnxobject.alternatives.push(altcnxobejct);
    	    	}
    	    	*/

    	    	tmp_itineraries[item].current += 1;
    		}
    	}

    	console.debug(this);
	}
};
