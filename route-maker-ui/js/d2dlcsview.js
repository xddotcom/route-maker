var BD_WORLD   = 0;
var BD_NETWORK = 1;
var BD_ITIN    = 2;
var BD_CNX     = 3;
var zoomTexts = [ "back", "on region", "on trip", "on line"];

var View = function (model) {
	var _this = this;
	this._model = model;

	this.allowAutoZooms = true;

	this._endsImage = {
		style: "bubble",
		O : "walk",
		D : "end"
	};
	this._stopsImage = "redsquare";
	this._icons = new IconManager();

	this._networks = [];

	this._itineraries = [];

	this._endMarkers = {"O":null, "D":null};
	this._ends = {"O":null, "D":null};

	this._allBounds = [null, null, null, null];
	this._allBoundsIndex = -1;

	this._polylines = [];
	this._polylinesVisible = false;
	this._markers = [];

	this._content = [];

	this._baseWidthoffset = 2;
	this._ratioLine = 150;

	this.walkColor = "#FFFFFF00";


	this.displayViaPoints = false;

	model.bind("limits", function(sender, data){
		_this.networksChanged(data);
	});

	model.bind("address", function(sender, data){
		_this.selectDetails(data);
	});

	model.bind("directions", function(sender, data){
		_this.showPath(data.path);
	});

	this._types = { "R" : "rail",
                    "W" : "walk",
                    "L" : "bus",
                    "B" : "bus",
                    "+" : "bus",
                    "S" : "subway",
                    "F" : "air",
                    "X" : "car"};

	//r = new RegExp("[\\d\\.\\\\/]+", "g");
	//this.defaultStatusString = "navigator: " + String(navigator.userAgent).replace(r, '');
	this.defaultStatusString = "&nbsp;";

	this.displayItinsHandler = function(event){
		debug("View::displayItins clicked");
		_this.setStatus("Displaying result...");
		_this.displayItinerary(this.value);
		_this.setStatus();
    };

    this.triggerDetailsHandler = function(event) {
		debug("View::triggerDetails clicked");
		_this.displayItineraryDetails();
	};

	this.triggerZoomHandler = function(event) {
		debug("View::triggerZoom clicked");
		_this.fitBounds();
	};

	/* ************** */


    var starts = {
         init: function(){
        	 /* no more interesting options */
        	 /*
        	 changeTheme("#optionsButton", "e");
        	 jQuery("#optionsButton").one("click", function(){
        		 changeTheme("#optionsButton", "a");
        	 });
        	 */
         },
         first_results: function(){
        	 changeTheme("#zoomButton", "e");
        	 jQuery("#zoomButton").one("click", function(){
        		 changeTheme("#zoomButton", "a");
        	 });
        	 changeTheme("#detailsButton", "e");
        	 jQuery("#detailsButton").one("click", function(){
        		 changeTheme("#detailsButton", "a");
        	 });
        	 changeTheme("#resultsButton", "e");
        	 jQuery("#resultsButton").one("click", function(){
        	 	 changeTheme("#resultsButton", "a");
        	 });
         }
    };
    var ends = {
         init: function(){  },
         first_results: function(){ }
    };
    var transitions = {};
    this.states = new StateMachine(starts, ends, transitions);
    this.states.next("init");
};

View.prototype = {
	setMap: function(map){
		var _this = this;
		this._map = map;
		google.maps.event.addListener(map, 'zoom_changed', function() {
			var zoom = map.getZoom();
			var fillOpacity = 1 / (2*(1+Math.exp(10*(zoom-10))));
			debug("Change opacity: " + fillOpacity + ", " + zoom);
			for(var net in _this._networks){
				if(_this._networks[net].polygons){
					for( var p in _this._networks[net].polygons ){
						_this._networks[net].polygons[p].setOptions( { fillOpacity: fillOpacity } );
					}
				}
			}
		});
	},
	reset: function(){
		this.resetBounds(BD_NETWORK);
	},
	setOptions: function(options){
		debug("View::setOptions");

		var oldTriggerDetails = this.triggerDetails;
		var oldDisplayItins = this.displayItins;

		this.wide = options["wide"];

		this.displayLimits = options["displayLimits"];
		this.displayItins = options["displayItins"];
		this.displayDetails = options["displayDetails"];
		this.displayStatus = options["displayStatus"];
		this.displayContext = options["displayContext"];
		this.displayInfo = options["displayInfo"];
		this.triggerDetails = options["triggerDetails"];
		this.triggerResults = options["triggerResults"];
	    this.fixedOrigin = options["fixedOrigin"];
	    this.sizeOfLine = options["sizeOfLine"];
	    this.triggerZoom = options["triggerZoom"];
	    this.lineNoOptions = options["lineNoOptions"];
	    this.oriDesFields = options["oriDesFields"];

	    if( oldTriggerDetails !== this.triggerDetails  && this.triggerDetailsHandler){
			jQuery(this.triggerDetails).unbind("click", this.triggerDetailsHandler);
			jQuery(this.triggerDetails).bind("click", this.triggerDetailsHandler);
	    }

	    if( oldDisplayItins !== this.displayItins && this.displayItinsHandler){
			jQuery(this.displayItins).unbind("change", this.displayItinsHandler);
			jQuery(this.displayItins).bind("change", this.displayItinsHandler);
	    }

	    if( oldDisplayItins !== this.displayItins && this.triggerZoomHandler){
			jQuery(this.triggerZoom).unbind("click", this.triggerZoomHandler);
			jQuery(this.triggerZoom).bind("click", this.triggerZoomHandler);
	    }

	    this.updateTriggers();
	    this.updateInfo();
	},
	isFixed: function(){
		return $(this.fixedOrigin).val();
	},
	networksChanged: function(data){

		var panelTable = null;
		if(this.displayLimits) {
			debug("displayLimits = " + this.displayLimits);
  	  		panelTable = document.getElementById(this.displayLimits);
		}

		this._content.length = 0;
		this._networks.length = 0;

		var totalBounds = new google.maps.LatLngBounds();

		this.resetBounds(BD_NETWORK);

  	  	for(var limitIdx in data){
  	  		var limit = data[limitIdx];
  			debug(limit);
  	  		var sw = limit.sw;
  	  		var ne = limit.ne;
  	  		var bounds = new google.maps.LatLngBounds(new google.maps.LatLng(sw.lat,sw.lon),
				                                      new google.maps.LatLng(ne.lat,ne.lon));

  			totalBounds = totalBounds.union(bounds);
  			var polygons = [];

  			if(limit.shape){
  				var shapes = limit.shape.split("#");
  				for(var shapeIndex in shapes ){
  					var shape = shapes[shapeIndex].split(";");
  					var path = [];
  					for(var coord in shape ){
  						var a =  shape[coord].split(",");
  						path.push( new google.maps.LatLng(a[0], a[1]) );
  					}
  					var polygon = new google.maps.Polygon ( {
				  		  	  			path: path
				  		  	  			,clickable: false
				  		  	  			,fillColor: "FireBrick"
				  		  	  		    ,fillOpacity: 0.25
				  		  	  		    ,strokeColor: "FireBrick"
				  		  	  		    ,strokeWeight: 0
				  		  	  		    ,strokeOpacity: 0.75
				  		  	  			,map: this._map } );
  					polygons.push(polygon);

  		  	  		new google.maps.Polyline ( {
  		  	  			path: path
  		  	  			,clickable: false
  		  	  		    ,strokeColor: "FireBrick"
  		  	  		    ,strokeWeight: 4
  		  	  		    ,strokeOpacity: 0.75
  		  	  			,map: this._map } );
  				}

  			} else {
  	  	  		var path = [ new google.maps.LatLng(ne.lat,sw.lon),
  	  	  			        new google.maps.LatLng(ne.lat,ne.lon),
  	  	  			        new google.maps.LatLng(sw.lat,ne.lon),
  	  	  			        new google.maps.LatLng(sw.lat,sw.lon),
  	  	  			        new google.maps.LatLng(ne.lat,sw.lon)];

  	  	  		new google.maps.Polyline( {
  	  	  			path: path
  	  	  			,strokeColor: "FireBrick"
  	  	  		    ,strokeWeight: 6
  	  	  		    ,strokeOpacity: 0.75
  	  	  			,map: this._map } );
  			}

  	  	    this._networks.push({  name:    limit.name
                                  ,content: limit.content
	                              ,bounds:  bounds
	                              ,polygons: polygons});


  	  		if(this.displayLimits) {
	  	  		var _this = this;
	  	  		addCell( panelTable,
	  	  				[limit.name],
	  	  				"detailrow",
	  	  				"detailrowcell",
	  	  				bounds,
	    		        function(){
	  	  					_this.panToBounds(this.object);
	  	  				});
  	  		}
  	  		if(this.wide && limit.content){
  	  			this._content.push(limit.name + "("+ limit.content + ")");
  	  		} else if(limit.content) {
  	  			this._content.push(limit.content);
  	  		} else if(limit.content) {
  	  			this._content.push(limit.name);
  	  		}
	    }

  		this.setBounds(BD_WORLD, totalBounds);
  		this.fitBounds();

  		debug(this._networks);

  	  	this.updateContext();
	},
	itinerariesChanged: function(data, status){

		debug("View::itinerariesChanged");
		this.setStatus("Receiving itineraries data ...");

		this.clearMap();

		this.updateContext(status.cpu, status.mem);

		this._itineraries.length = 0;

		removeAll(this.displayItins);

		for(var itinIdx in data){
			var itin = data[itinIdx];

			itin.args = to_map(itin.free_txt);
			itin.score = 111;

			var optims = itin.args["opt"];
			if( optims ){
				for(var oneOpt in optims){
					switch(optims[oneOpt]){
					case "W": itin.score -= 1; break;
					case "X": itin.score -= 10; break;
					case "C": itin.score -= 100; break;
					}
				}
			}

			this._itineraries.push(itin);
		}

		this._itineraries.sort(function(a,b){return a.score - b.score;});

		debug("  Number of itineraries: " + this._itineraries.length);

		if(this._itineraries.length > 0){
		    this.states.tr("init", "first_results");
		}

		if(this.displayItins) {
			if(this._itineraries.length > 0){
				for(var itinIdx in this._itineraries){
					var _this = this;
					var itin = this._itineraries[itinIdx];
					var txtLines = this.buildItineraryText(itin);
					itin.listItem = this.addWithListener(this.displayItins,
							                             implode(txtLines),
							                             null,
							                             itinIdx,
							                             function(index){
										    				_this.displayItinerary(index);
										 			     } );
				}
			}
		}

		if(this._itineraries.length > 0){
	        this.displayItinerary(0);
		}

		/* update UI */
		this.updateUI();

		this.updateTriggers();

		this.displayError(status.error);
	},
	buildItineraryText: function(itin){
		var txtLines = [];

		var nbChanges = 0;
		var duration = 0;
		var walkTime = 0;
		var optims = null;

		for(var key in itin.args){
			switch(key){
			case "chg": nbChanges = parseInt(itin.args[key]); break;
			case "wlk": walkTime = parseInt(itin.args[key]); break;
			case "cos": duration = parseInt(itin.args[key]); break;
			case "opt": optims = itin.args[key]; break;
			}
		}

		var dep = null;
		var arr = null;
		if( itin.connexions.length > 0 ){
			var fConx = itin.connexions[0];
			var lConx = itin.connexions[itin.connexions.length-1];
			dep = fConx.departure;
			arr = lConx.arrival;
			duration = (arr.getTime() - dep.getTime())/1000 + walkTime;
		}

		var changeLine = "Direct";
		if(nbChanges > 0){
			changeLine = (nbChanges+1) + "&nbsp;trips";
		}

		var costLine = timeFormat(duration, 1, 1, 0);

		var walkLine = (this.wide?"walk&nbsp;time&nbsp;~":"walk&nbsp;") + timeFormat(walkTime, 1, 5, 0);


		var  changeLineStyle = "float:left; display: block; width: 29%; margin: 0 2%; white-space: normal; text-align: center; font-size: 0.9em;";
		var  costLineStyle   = "float:left; display: block; width: 29%; margin: 0 2%; white-space: normal; text-align: center; font-size: 0.9em;";
		var  walkLineStyle   = "float:left; display: block; width: 29%; margin: 0 2%; white-space: normal; text-align: center; font-size: 0.9em;";

		var  departureStyle  = "float:left; display: block; width: 46%; margin: 0 2%; white-space: nowrap; text-align: right; font-size: 0.9em; color: #39AC73";
		var  arrivalStyle    = "float:right; display: block; width: 46%; margin: 0 2%; white-space: nowrap; text-align: left; font-size: 0.9em; color: #39AC73";

		for(var oneOpt in optims){
			switch(optims[oneOpt]){
			case "W": walkLine   = (this.wide?"minimum&nbsp;":"min&nbsp;") + walkLine; break;
			case "X": changeLine = (this.wide?" minimum&nbsp;number of&nbsp;changes: ":"") + changeLine; break;
			case "C": costLine   = (this.wide?"quickest solution: ":"") + costLine + (this.wide?"":" quickest"); break;
			}
		}

		for(var oneOpt in optims){
			switch(optims[oneOpt]){
			case "W": walkLineStyle   += "color: #CC4D33; font-size: 1em;"; break;
			case "X": changeLineStyle += "color: #CC4D33; font-size: 1em;"; break;
			case "C": costLineStyle   += "color: #CC4D33; font-size: 1em;"; break;
			}
		}

		if( dep && arr){
			txtLines.push("<div style='"+departureStyle+"'>departure " + dateFormatAsTime(dep) + "</div>");
			txtLines.push("<div style='"+arrivalStyle  +"'>arrival " + dateFormatAsTime(arr) + "</div>");
		}

		txtLines.push("<div style='clear:both; " + changeLineStyle + "'>" + changeLine + "</div>");
		txtLines.push("<div style='" + costLineStyle   + "'>" + costLine   + "</div>");
		txtLines.push("<div style='" + walkLineStyle   + "'>" + walkLine   + "</div>");


		return txtLines;
	},
	displayItinerary: function (itineraryIndex){
		debug("View::displayItinerary("+itineraryIndex+")");
		var _this = this;

		this.clearMarkers();

		if(itineraryIndex >= this._itineraries.length){
			return;
		}

		if(this.currentItineraryIndex >= 0 && this.currentItineraryIndex < this._itineraries.length){
			var prevItinerary = this._itineraries[this.currentItineraryIndex];
			debug("remove border for #"+prevItinerary.listItem);
			changeTheme("#"+prevItinerary.listItem, "d");
		}

		var itinerary = this._itineraries[itineraryIndex];

		var width = jQuery(document).width();
		var height = jQuery(document).height();
		if(this.sizeOfLine){
			this._ratioLine = jQuery(this.sizeOfLine).val();
		}
		this._baseWidth = Math.max(parseInt(width/this._ratioLine),parseInt(height/this._ratioLine));

		var currentPoint = this._ends["O"];
		var dpoint = new google.maps.LatLng(currentPoint.lat,currentPoint.lon);

		var showEndLabel = this.wide;
		var showIcons = false;
		var noLabel = (!showEndLabel);

		var cnx = null;
		for(var index in itinerary.connexions){
			cnx = itinerary.connexions[index];

			/*
			 * Origin
			 */
			var opoint = new google.maps.LatLng(cnx.origin.lat,cnx.origin.lon);
			dpoint = new google.maps.LatLng(cnx.destination.lat,cnx.destination.lon);

			if(currentPoint.code != cnx.origin.code){
				var cpoint = new google.maps.LatLng(currentPoint.lat,currentPoint.lon);
				this._model.getDirections(cpoint, opoint);
			}

			if(showIcons){
				this.addMarker( opoint
				                ,cnx.origin.name
				                ,this._types[cnx.type]
				                ,noLabel?null: dateFormatAsTime(cnx.departure) + " - " + cnx.origin.name
		        		        ,noLabel?null: cnx.color
		        		        ,noLabel?null: cnx.txtcolor);
			}
			noLabel = true;

			var allCnxToDisplay = [cnx];
			if(isset(cnx.alternatives) && cnx.alternatives.length > 0 ){
				//allCnxToDisplay = cnx.alternatives;
				allCnxToDisplay = [cnx.alternatives[0]];
			}

			for(var allCnxToDisplayPos in allCnxToDisplay){
				var subcnx =  allCnxToDisplay[allCnxToDisplayPos];

				/*
				 * via points
				 */
				var points = [ opoint ];
				if( subcnx.via ){
					for(var viaIdx in subcnx.via){
						var viap = subcnx.via[viaIdx];
				    	var vpoint = new google.maps.LatLng(viap.lat,viap.lon);
				    	points.push(vpoint);
				    	if(this.displayViaPoints) {
							this.addMarker( vpoint,
									        viap.name,
							        	    "marker");
				    	}
				    }
				}
				points.push(dpoint);

				if( subcnx.type == "W" ){
					var polyline = this.addPolyline(points,"green",1,this._baseWidth/2);
					subcnx.polyline = polyline;
				} else {
					/*
					 * draw lines
					 */
					var polyline = this.addPolyline(points,
					                                "white",
					                                0.5,
					                                this._baseWidth+this._baseWidthoffset * 4);

					google.maps.event.addListener(polyline, 'click', function() {
						_this.highLightPolyline(this);
					});

					var subPoly = this.addPolyline(points,
							                       "black",
							                       1,
							                       this._baseWidth+this._baseWidthoffset);

					subPoly.highlight = polyline;
					google.maps.event.addListener(subPoly, 'click', function() {
						_this.highLightPolyline(this.highlight);
					});

					var subSubPoly = this.addPolyline(points,
							                          subcnx.color,
							                          1,
							                          this._baseWidth);

					subSubPoly.highlight = polyline;
					google.maps.event.addListener(subSubPoly, 'click', function() {
						_this.highLightPolyline(this.highlight);
					});

					subcnx.polyline = polyline;


					var longtext = "";
					var shorttext = "";

					if(isset(subcnx.route)){
						longtext = subcnx.route + ": ";
						shorttext = subcnx.route + " - ";
					} else if(isset(subcnx.route)){
						longtext = subcnx.operator + ": ";
						longtext = subcnx.route + " - ";
					}

					longtext += subcnx.origin.name + " - " + subcnx.destination.name;

					if(isset(subcnx.reference)){
						longtext += " (" + subcnx.reference + ")";
						shorttext += subcnx.reference;
					}

					this.addMarker( subSubPoly.getCenter()
			                        ,longtext
			                        ,this._types[subcnx.type]
									,shorttext
									,showEndLabel?subcnx.color:null
			        				,showEndLabel?subcnx.txtcolor:null);
				}

				currentPoint = subcnx.destination;

			}


		}
		var destination = this._ends["D"];
		if(currentPoint.code != destination.code){
			var epoint = new google.maps.LatLng(destination.lat,destination.lon);
			this._model.getDirections(dpoint, epoint);
		}
		if(showIcons && isset(cnx)){
			this.addMarker( dpoint
			                ,cnx.destination.name
			                ,"walk"
					        ,showEndLabel?dateFormatAsTime(cnx.arrival) + " - " + cnx.destination.name:null
			        		,showEndLabel?cnx.color:null
	        				,showEndLabel?cnx.txtcolor:null);
		}

		this.setVisible(true);

		this.currentItineraryIndex = itineraryIndex;

		this.updateUI();

		debug("set border for #"+itinerary.listItem);
		changeTheme("#"+itinerary.listItem, "e");
	},
	displayItineraryDetails: function (itineraryIndex){

		if(!itineraryIndex){
			itineraryIndex = this.currentItineraryIndex;
		}

		if(itineraryIndex === this.currentItineraryIndexDetails ){
			return;
		}
		this.currentItineraryIndexDetails = itineraryIndex;


		removeAll(this.displayDetails);

		if(itineraryIndex >= this._itineraries.length){
			return;
		}

		var itinerary = this._itineraries[itineraryIndex];

		var currentPoint = this._ends["O"];
		var dpoint = new google.maps.LatLng(currentPoint.lat,currentPoint.lon);
		var style;

		for(var index in itinerary.connexions){
			var cnx = itinerary.connexions[index];
			/*
			 * Origin
			 */
			var opoint = new google.maps.LatLng(cnx.origin.lat,cnx.origin.lon);

			if(currentPoint.code != cnx.origin.code){
				var cpoint = new google.maps.LatLng(currentPoint.lat,currentPoint.lon);
				var directions = this._model.getDirectionsSync(cpoint, opoint);
				this.makeWalkDetails(currentPoint, cnx.origin, directions);
			}

			noLabel = true;

			/*
			 * destination
			 */
			dpoint = new google.maps.LatLng(cnx.destination.lat,cnx.destination.lon);

			/*
			 * add cell
			 */
			var _this = this;

			if( cnx.type == "W" ){
				this.makeWalkDetails(currentPoint, cnx.origin, { duration: cnx.duration });
			} else {
				var desc = "";
				if(isset(cnx.operator)){
					desc += cnx.operator;
				}
				desc += " ";
				if(isset(cnx.reference)){
					desc += cnx.reference;
				}
				var txt = [ desc,
				            cnx.route,
							dateFormatAsTime(cnx.departure) + " - " + cnx.origin.name,
						    dateFormatAsTime(cnx.arrival) + " - " + cnx.destination.name];

				style = "border-left:10px "+cnx.color+" solid; color: "+cnx.txtcolor + "; padding-top:1px;padding-bottom:1px;";
				this.addWithListener(this.displayDetails,
						             this.displayItineraryDetailsText(txt,this._types[cnx.type]),
							 		 style,
							 		 cnx.polyline,
					                 function(value){
							    	   _this.highLightPolyline(value);
							 		 });
			}

			currentPoint = cnx.destination;
		}

		var destination = this._ends["D"];
		if(currentPoint.code != destination.code){
			var epoint = new google.maps.LatLng(destination.lat,destination.lon);
			var directions = this._model.getDirectionsSync(dpoint, epoint);
			this.makeWalkDetails(currentPoint, destination, directions);
		}

		this.updateUI();
	},
	makeWalkDetails: function(fromPoint, toPoint, directions){
		var txt = [ "from " + fromPoint.name,
		            "to " + toPoint.name ];
		var first = [];
		if(directions && directions.distance) {
			first.push(smartRound(directions.distance) + " meters");
			if(directions.distance < 100){
				/* less than 100 meters  */
				return ;
			}
		}
		if(directions && directions.duration) {
			first.push(timeFormat(directions.duration, 1, 1, 0, " hours ", " minutes "));
			if(directions.distance < 60){
				/* less than one minute */
				return ;
			}
		}
		if(first.length > 0){
			txt.unshift(implode(first, ", "));
			txt.unshift("Walk");
			style = "border-left:10px "+this.walkColor+" solid; padding-top:1px;padding-bottom:1px;";
			this.addWithListener(this.displayDetails,
					             this.displayItineraryDetailsText(txt,this._types["W"]),
					 			 style);
		}
	},
	displayItineraryDetailsText: function(txt,type){
		if(this.wide){
			var icon = "<td width='10%' rowspan='2'><img src='"+this._icons.getImageURL(this._stopsImage,type)+"'/></td>";
			var s1 = "<td width='30%' rowspan='2'>"+txt[0]+"</td>";
			var s2 = "";
			if(isset(txt[1])){
				s1 = "<td width='30%' >"+txt[0]+"</td>";
				s2 = "<td width='30%' >"+txt[1]+"</td>";
			}
			var s3 = "<td width='60%' >"+txt[2]+"</td>";
			var s4 = "<td width='60%' >"+txt[3]+"</td>";
			return "<table width='100%'><tr>"+ icon + s3 + s1 + "</tr><tr>" + s4 + s2 + "</tr></table>";
		}
		return "<p>" + implode(txt, "</p><p>") + "</p>";
	},
	showPath: function(path){
		this.addPolyline(path, "green", 1, 3);
	},
	addPolyline: function(path, color, opacity, weight ){
    	var polyline = new google.maps.Polyline({
            path: path,
            strokeColor: color,
            strokeOpacity: opacity,
            strokeWeight: weight } );

		for(var i in path){
			this.addToBounds(BD_ITIN, path[i]);
		}

    	this._polylines.push(polyline);

		if(this._polylinesVisible){
			polyline.setMap(this._map);
		} else {
			polyline.setMap(null);
		}

		return polyline;
	},
	highLightPolyline: function(polyline){
		debug("View::highLightPolyline");
		if(this._currentPolyline){
			this._currentPolyline.setOptions({strokeColor: "white"});
		}
		if(this._currentPolyline === polyline){
			this.resetBounds(BD_CNX);
			this._currentPolyline = null;
		} else {
			this._currentPolyline = polyline;
			if(this._currentPolyline){
				this.resetBounds(BD_CNX);
				this._currentPolyline.setOptions({strokeColor: "red"});
				var path = this._currentPolyline.getPath().getArray();
				for(var i in path){
					this.addToBounds(BD_CNX, path[i]);
				}
			}
		}
	},
	addMarker: function(point,
                        text,
                        icon,
                        label,
                        color,
                        textcolor){

		var options = {
				 point     : point
                ,text      : text
                ,image     : this._icons.getImage(this._stopsImage, icon)
                ,shadow    : this._icons.getShadow(this._stopsImage, icon)
                ,label     : label
                ,color     : color
                ,textcolor : textcolor
                ,zIndex    : 9999
		};

		var marker = makeMarker(options);

		this._markers.push(marker);

		this.addToBounds(1,point);
	},
	setVisible: function(isVisible) {
		debug("View::setVisible("+isVisible+")");
		this._polylinesVisible = isVisible;
		for(var pidx in this._polylines){
			if(isVisible){
				this._polylines[pidx].setMap(this._map);
			} else {
				this._polylines[pidx].setMap(null);
			}
		}
		for(var midx in this._markers){
			if(isVisible){
				this._markers[midx].setMap(this._map);
			} else {
				this._markers[midx].setMap(null);
			}
		}
	},
	clearMarkers: function(){
		debug("View::clearMarkers");
		this.setVisible(false);
		this._polylines.length = 0;
		this._currentPolyline = null;
		this._markers.length = 0;

		this.resetBounds(BD_ITIN);

		if(this._ends.O && this._ends.D){
			this.addToBounds(BD_ITIN, new google.maps.LatLng(this._ends.O.lat,this._ends.O.lon));
			this.addToBounds(BD_ITIN, new google.maps.LatLng(this._ends.D.lat,this._ends.D.lon));
		}
	},
	clearMap: function(error){
		debug("View::clearMap(error="+error+")");
		/* no itineraries */
		this._itineraries.length = 0;
		this.currentItineraryIndexDetails = -1;
		this.currentItineraryIndex = -1;
		/* remove all markers */
		this.clearMarkers();
		/* reset list of results */
		if(error){
			this.displayError(error);
		}
		/* reset details */
		removeAll(this.displayDetails);
		this.updateTriggers();
		this.updateContext();
	},
	addToBounds: function(bounds, point){
		if(!this._allBounds[bounds]){
			this._allBounds[bounds] = new google.maps.LatLngBounds();
		}
		this._allBounds[bounds].extend(point);
		this.updateZoomButton();
	},
	setBounds: function(bounds, bound){
		debug("View::setBounds("+bounds + ", " + bound +")");
		this._allBounds[bounds] = bound;
		this.updateZoomButton();
	},
	resetBounds: function(bounds){
		for(var i = bounds; i < this._allBounds.length; i++){
			this._allBounds[i] = null;
		}
		//this._allBoundsIndex = (bounds == 0 ? 0 : bounds-1);
		this.updateZoomButton();
	},
	fitBounds: function(){
		debug("View::fitBounds("+this._allBoundsIndex+")");
		var manualZoom = false;
		if( manualZoom ){
			/* we have manually zoomed : rezoom to the current zoom level  */
		}
		else{
			this._allBoundsIndex = (this._allBoundsIndex+1) % this._allBounds.length;
		}
		if(this._allBounds[this._allBoundsIndex] === null){
			this._allBoundsIndex = BD_WORLD;
		}
		if(this._allBoundsIndex == BD_WORLD && this._allBounds[BD_NETWORK] !== null ){
			this._allBoundsIndex = BD_NETWORK;
		}
		if(this._allBounds[this._allBoundsIndex] !== null){
			this._map.fitBounds(this._allBounds[this._allBoundsIndex]);
		}
		this.updateZoomButton();
		debug("    -> View::fitBounds("+this._allBoundsIndex+")");
	},
	detectBounds: function(){
		var mapBounds = this._map.getBounds();
		debug("View::detectBounds("+ mapBounds  +")");
		var lastLarger = 0;
		var firstSmaller = 0;
		if(mapBounds){
			for(var i = 0; i < this._allBounds.length; i ++){
				if(this._allBounds[i] === null){
					debug( "skip " + i);
					continue;
				}
				if( this._allBounds[i].includes(mapBounds) ){
					lastLarger = i;
				} else if( mapBounds.includes(this._allBounds[i]) ){
					firstSmaller = i;
					break;
				}
			}
		}
		return [lastLarger, firstSmaller];
	},
	updateZoomButton: function() {
		/* who's next ? */

		/*
		var nextIndex = this._allBoundsIndex;
		for(var i = 0; i < this._allBounds.length; i ++){
			if(this._allBounds[nextIndex]){
				break;
			}
			nextIndex = (nextIndex+1) % this._allBounds.length;
		}
		jQuery(this.triggerZoom).find(".ui-btn-text").text((this.wide?"zoom ":"")+zoomTexts[nextIndex])
		*/
	},
	setStatus: function(status){
		if(this.displayStatus){
			if(!status || status.length==0){
				status = this.defaultStatusString;
			}
			jQuery(this.displayStatus).html(status);
			this._currentStatus = status;
		}
	},
	updateContext: function(cpu, memory){
		if(this.displayContext){
			/* We're writing context, info that should remain ; delete status */
			this.setStatus();
			/* now write context */
			var innerHTML = [];
			if(cpu){
				innerHTML.push("CPU"+(this.wide?" time":"")+": " + cpu + " ms");
			}
			if(memory){
				innerHTML.push("Mem"+(this.wide?"ory":"") +": "+ memory + " Mo");
			}
			if(innerHTML.length == 0 || this.wide){
				innerHTML.unshift(this._content.join(", "));
			}
			jQuery(this.displayContext).html(implode(innerHTML, " - "));
		}
	},
	updateInfo: function(){
		debug("View::updateInfo");
		if( this.displayInfo ){

			var origin = jQuery("#origin");
			var destination =  jQuery("#destination");
			var txt = "";
			var withOptions = false;

			if( this._ends.O ){
				withOptions = true;
				origin.val(this._ends.O.name);
				debug( "  origin is now: " + origin.val() );
				if( this.isFixed() == "origin"){
					txt += "<u>"+this._ends.O.shortName + "</u>";
				} else {
					txt += this._ends.O.shortName;
				}
			} else {
				origin.val("");
			}
			if( this._ends.D ){
				withOptions = true;
				destination.val(this._ends.D.name);
				debug( "  destination is now: " + destination.val() );
				if(this.wide){
					txt += "<span style=\"padding-left:20px; padding-right:20px\">&rarr;</span>";
				} else {
					txt += "&nbsp;&rarr;&nbsp;";
				}
				if( this.isFixed() == "destination"){
					txt += "<u>" + this._ends.D.shortName + "</u>";
				} else {
					txt += this._ends.D.shortName;
				}
			} else {
				destination.val("");
			}

			if( this._ends.D && this._ends.O ){
				this.addToBounds(BD_ITIN, this._ends.O.point);
				this.addToBounds(BD_ITIN, this._ends.O.point);
			}


			if(this.wide){
				txt += "<br/>";//<span style=\"padding-left:20px; padding-left:20px\">Departure: </span>";
				txt += jQuery("#departureTime").val();
			}

			if( withOptions ){
				jQuery(this.displayInfo).html(txt);
			} else {
				jQuery(this.displayInfo).html(this.lineNoOptions);
			}
		}
	},
	showOrigin: function(point, name, address) {
		debug("View::showOrigin(point=" + point+", name=" + name+", address="+ address+")");
		this.showEnd("O", point, "", "__ori");
		if(point && name && address){
			this.showDetails("O", point, name, address);
		}
	},
	showDestination: function(point, name, address) {
		debug("View::showDestination(point=" + point+", name=" + name+", address="+ address+")");
		this.showEnd("D", point, "", "__des");
		if(point && name && address){
			this.showDetails("D", point, name, address);
		}
	},
	showEnd: function(markerName, point, name, code) {
		debug("View::showEnd(markerName="+markerName+", name="+name+", code="+code+")");
		/* remove existing */
		if(this._endMarkers[markerName]) {
			this._endMarkers[markerName].setMap(null);
		}
		this._endMarkers[markerName] = null;
		this._ends[markerName] = null;
		/* create */
		if(point) {

			var options = {
					 point : point
					,name  : name
					,image : this._icons.getImage(this._endsImage.style, this._endsImage[markerName])
					,shadow: this._icons.getShadow(this._endsImage.style, this._endsImage[markerName])
					,zIndex: 10000
			};
			this._endMarkers[markerName] = makeMarker(options);
			this._endMarkers[markerName].setMap(this._map);
			this._ends[markerName] = { lat:  point.lat(),
					                   lon:  point.lng(),
					                   point: point,
					                   name: name,
					                   shortName: name,
							           code: code };

			for( var netIndex in this._networks ){
				if( this._networks[netIndex].bounds.contains(point) ){
					debug("Network " + this._networks[netIndex].name);
					this.setBounds(BD_NETWORK, this._networks[netIndex].bounds);
					break;
				}
			}
		}
		this.updateInfo();
	},
	selectDetails: function(results){
		debug("View::selectDetails()");
		var index = 0;
		var data = results[index];
		this.showDetails(data.arg,
                         data.point,
                         data.name,
                         data.address);
        jQuery(this.oriDesFields[data.arg]).one('focus', function(event) {
            jQuery(this).val("");
        });
	},
	showDetails: function(markerName, point, name, address) {
		debug("View::showDetails('"+markerName+"', point="+point+", name="+name+", address="+address+")");
		var marker = this._endMarkers[markerName];
		var end = this._ends[markerName];

		if(marker && address){
			marker.setTitle(address);
		}
		if(end && address){
			end.name = address;
			if( this.wide){
				a = address.split(",");
				a = a.splice(a, a.length-1);
				for( var idx = 0; idx < a.length; idx += 1 ) {
					a[idx] = a[idx].ltrim('[\\s\\d-]');
				}
				end.shortName = implode(a, ", ");
			} else {
				end.shortName = name.split(",")[0].ltrim('[\\s\\d-]');
			}
		}
		this.updateInfo();
	},
	getDateTime: function(){
		return jQuery("#departureTime").scroller('getDate');
	},
	updateTriggers: function() {
		 jQuery(this.triggerResults).find(".ui-icon").text(this._itineraries.length);
	},
	displayError: function(error){
		debug("View::displayError(error="+error+"), " + this._itineraries.length);
		/* display error */
		if(error){
			jQuery.mobile.changePage("#dialog-message", null, true, true);
			jQuery("#error_message").text(error);
		}
	},
	addWithListener: function(list, html, style, data, listener) {
		var tag = new Date().getTime()+"_"+Math.floor(Math.random()*1000000);
		var li = "li_"+tag;
		var id = "a_"+tag;
		if(!style){
			style = "";
		}
        if(listener){
        	debug("Create li "+id);
            jQuery(list)
            	.append("<li id='"+li+"' style='"+style+"'><a href='#main' style='padding-top:3px;padding-bottom:3px' id='"+id+"'>"+html+"</a></li>");
            getElementById(id)._value = data;
            jQuery("#"+id).click(function(event){
            	listener(this._value, li);
            });
        } else {
            $(list).append("<li style='"+style+"'>"+html+"</li>");
        }
        return li;
	},
	updateUI: function(){
        try{
        	$(this.displayItins).listview('refresh');
        } catch (err) {
        	debug(err);
        }
        try{
        	$(this.displayDetails).listview('refresh');
        } catch (err) {
        	debug(err);
        }
	},
	chooseAmong: function(data, formatter, listener){
		removeAll("#dialog-choose-list");
		for(var index in data ){
            $("#dialog-choose-list").append('<input type="radio" name="radio-choice" id="radio-choice-'+index+'" value="'+index+'" />');
			$("#dialog-choose-list").append('<label for="radio-choice-'+index+'">' + formatter(data[index]) + '</label>');
		}
		$("#dialog-choose-button-ok").one("click", function(){
			var index = getSelectedInRadios("#dialog-choose-list");
			if(index >= 0){
				listener(index);
			}
		});
		$("#dialog-choose").trigger ("create");
		$.mobile.changePage("#dialog-choose", null, true, true);
	}
};
