var Controller = function (view, model, options) {
	this._view = view;
	this._model = model;
	this._origin = null;
	this._destination = null;

	var _this = this;

	this.resetFunction = function(event){
		debug("Controller::triggerReset clicked");
		_this.selectedOrigin(null);
		_this.selectedDestination(null);
		_this._view.reset();
    };

    this.reloadFunction = function(event, ui) {
		_this.reloadItineraries();
	};

	model.bind("position", function(sender, data){
		if(data.length > 1){
			_this._view.chooseAmong(
				data,
				function(item){
					return "<p>" + item.locality + " " + item.postal_code + "</p><small style='padding-left:20px'>" + item.address + "</small>";
				},
				function(index){
					_this.selectedPosition(data[index]);
				});
		} else if(data.length == 1) {
			_this.selectedPosition(data[0]);
		}
	});

	model.bind("itineraries", function(sender, data){
		var errno = parseInt(data.status.errno);
		if( isset(errno) && errno == 3 ){
			debug("Call solutions");
			_this.reloadSolutions();
		} else {
			_this._view.itinerariesChanged(data.itineraries, data.status);
		}
	});

	model.bind("solutions", function(sender, data){
		_this._view.itinerariesChanged(data.itineraries, data.status);
	});

	this.setOptions(options);
};

Controller.prototype = {
	setMap: function(map){
		var _this = this;
		this._view.setMap(map);
		google.maps.event.addListener(map, 'click', function(event){
			_this.mapClicked(event);
		});
	},
	setOptions: function(options){
		var _this = this;

		this.options = options;

		debug( options.continuous );

		for(var idx in options.resetButtons){
			jQuery(options.resetButtons[idx]).bind("click", this.resetFunction);
		}


		var originCont = jQuery.inArray(options.origin, options.continuous) >= 0;
		jQuery(options.origin).change(function(event) {
			_this.endSelected("O",
					          jQuery(_this.options.origin).val(),
					          originCont);
		});

		var destinationCont = jQuery.inArray(options.destination, options.continuous) >= 0;
		jQuery(options.destination).change(function() {
			_this.endSelected("D",
					          jQuery(_this.options.destination).val(),
					          destinationCont);
		});

		if( jQuery.inArray(options.departureTime, options.continuous) >= 0 ){
			jQuery(_this.options.departureTime).change(this.reloadFunction);
		}

		if( jQuery.inArray(options.nbMaxChanges, options.continuous) >= 0 ){
			jQuery(options.nbMaxChanges).bind( "change", function(event, ui) {
				debug("Controller::nbMaxChanges called " + jQuery(_this.options.nbMaxChanges).val());
				_this.reloadItineraries();
			});
		}

		if( jQuery.inArray(options.maxWalkInM, options.continuous) >= 0 ){
			jQuery(options.maxWalkInM).bind( "change", function(event, ui) {
		    	debug("Controller::maxWalkInM called " + jQuery(_this.options.maxWalkInM).val());
				_this.reloadItineraries();
			});
		}

		for(var idx in options.backButtons){
			jQuery(options.backButtons[idx]).bind("click", this.reloadFunction);
		}
	},
	mapClicked: function(event) {
		point = event.latLng;
		var fixOrigin = (this._view.isFixed()=="origin");
		var fixDestination = (this._view.isFixed()=="destination");

		debug(fixOrigin + ", " + fixDestination);

		if(this._origin===null) {
			/* origin is to be set */
			this.selectedOrigin(point);
		} else if(this._destination===null) {
			/* origin is to be set */
			this.selectedDestination(point);
		} else {
			if( fixOrigin ) {
				this.selectedDestination(point);
			} else if( fixDestination ){
				this.selectedOrigin(point);
			} else {
				/* we are changing origin (first time origin, second time destination) */
				this.selectedDestination(null, true); // reset it , we are going to change is at next click
				this.selectedOrigin(point);
			}
		}
	},
	reloadItineraries: function(){
		debug("Controller::reloadItineraries");
		var datetime = this._view.getDateTime();
		var date = dateFormatAsDate(datetime);
		var time = dateFormatAsTime(datetime);
		var nbChanges = jQuery(this.options.nbMaxChanges).val();
		var maxWalkInM = jQuery(this.options.maxWalkInM).val();
		var optims = ["COST"];

		for(var optim in this.options.optims){
			if( jQuery(this.options.optims[optim]).val() !== "off" ){
				optims.push(optim);
			}
		}

		if(this._model.loadItineraries( this._origin,
				                        this._destination,
				                        date,
				                        time,
				                        nbChanges,
				                        maxWalkInM,
				                        optims)){
			this.setStatus("Querying server ...");
		} else {
			this._view.clearMap();
		}
	},
	reloadSolutions: function(){
		debug("Controller::reloadSolutions");
		var datetime = this._view.getDateTime();
		var date = dateFormatAsDate(datetime);
		var time = dateFormatAsTime(datetime);
		var nbChanges = jQuery(this.options.nbMaxChanges).val();
		var maxWalkInM = jQuery(this.options.maxWalkInM).val();
		var optims = ["COST"];

		for(var optim in this.options.optims){
			if( jQuery(this.options.optims[optim]).val() !== "off" ){
				optims.push(optim);
			}
		}

		if(this._model.loadSolutions( this._origin,
				                      this._destination,
				                      date,
				                      time,
				                      nbChanges,
				                      maxWalkInM,
				                      optims)){
			this.setStatus("Querying server ...");
		} else {
			this._view.clearMap();
		}
	},
	selectedOrigin: function(point, no_reload, name, address){
		debug("Controller::selectedOrigin: " + point);
		this._origin = point;

		this._view.clearMap();
		this._view.showOrigin(this._origin, name, address);

		if(address==null||name==null){
			/*
			 * We have no info, thus we call google
			 */
			this._model.getAddress(point, "O");
		}

		if(!no_reload){
			this.reloadItineraries();
		}
	},
	selectedDestination: function(point, no_reload, name, address){
		debug("Controller::selectedDestination: " + point);
		this._destination = point;
		this._view.showDestination(this._destination, name, address);

		if(address==null||name==null){
			/*
			 * We have no info, thus we call google
			 */
			this._model.getAddress(point, "D");
		}

		if(!no_reload){
			this.reloadItineraries();
		}
	},
	selectedPosition: function(data){
		debug("Controller::selectedPosition: " + data.arg + ", position=" + data.point + ", address=" + data.address);
		if(data.arg.end == "O"){
			this.selectedOrigin(data.point,
					            !data.arg.reload,
					            data.name,
					            data.address);
		} else if(data.arg.end == "D"){
			this.selectedDestination(data.point,
	                                 !data.arg.reload,
					                 data.name,
					                 data.address);
		}
	},
	endSelected: function(end, message, reload){
		debug("Controller::endSelected: " + end + ", message = '" + message + "', reload='"+reload+"'");
		this._model.getPosition(message, {end:end, reload:reload});
	},
	loadNetworks: function(){
		this._model.loadNetworks();
	},
	getStatus: function(){
		this._model.getStatus();
	},
	setStatus: function(status){
		this._view.setStatus(status);
	}


};