/**
 *
 * @param point
 * @param text
 * @param label
 * @param color
 * @param textcolor
 * @param draggable
 * @param callback
 * @param minZoom
 * @returns MarkerWithLabel
 */
function makeMarker(options) {
	var marker;

	var point = options.point;

	if(!point){
		return null;
	}

	var text = options.text;
	var image = options.image;
	var shadow = options.shadow;
	var label = options.label;
	var zIndex = options.zIndex;

	if(label) {

		var color      = options.color;
		var textcolor  = options.textcolor;
		var draggable  = options.draggable;
		var callback   = options.callback;
		var minZoom    = options.minZoom;

		if (color==null) {
			color = "white";
			textcolor = "black";
		}

		marker = new MarkerWithLabel ({
			title: text,
			zIndex: zIndex,
			draggable: draggable,
			raiseOnDrag: false,
			icon: image,
			shadow: shadow,
			position: point,
			labelContent: label,
			labelAnchor: new google.maps.Point(-5, 30),
			labelClass: "labels",
			labelInBackground: false,
			labelStyle: {
				"color": textcolor,
				"backgroundColor": color }
			});

		if(minZoom){
			marker.minZoom = minZoom;
		} else {
			marker.minZoom = 0;
		}
		if(draggable){
			google.maps.event.addListener(marker, 'dragend', function () {callback(marker);});
		}
	} else {
		marker = new google.maps.Marker({
			title: text,
			icon: image,
			shadow: shadow,
			position: point,
			zIndex: zIndex});
	}
	return marker;
}



var IconManager = function(){
	this._icons = {};
	this._options = {
			bubble : {
				pos: new google.maps.Point(16,36),
				size: new google.maps.Size(32,36),
				shadow: true,
				ssize: new google.maps.Size(54,36),
			},
			sticker: {
				pos: new google.maps.Point(18,18),
				size: new google.maps.Size(36,36),
			},
			redsquare: {
				pos: new google.maps.Point(18,18),
				size: new google.maps.Size(36,37),
			}
	};
};

IconManager.prototype = {
	has: function(style, name){
		return this._icons[style] && this._icons[style][name] && this._icons[style][name].image;
	},
	buildIcon: function(style, name){
		var options = this._options[style];

		if(!this._icons[style]){
			this._icons[style] = {  };
		}
		if(!this._icons[style][name]){
			this._icons[style][name] = { image:null, shadow:null };
		}

		var url = 'images/'+style+"/"+name+'.png';
		this._icons[style][name]["imageURL"] = url;
		this._icons[style][name]["image"] = new google.maps.MarkerImage(
			url,
			options.size,
		    new google.maps.Point(0,0),
		    options.pos
		);

		if(options.shadow){
			this._icons[style][name]["shadow"] = new google.maps.MarkerImage(
				'images/'+style+'/_shadow.png',
				options.ssize,
			    new google.maps.Point(0,0),
			    options.pos
			);
		}
	},
	getImage: function(style, name){
		if( !this.has(style,name) ){
			this.buildIcon(style, name);
		}
		return this._icons[style][name].image;
	},
	getImageURL: function(style, name){
		if( !this.has(style,name) ){
			this.buildIcon(style, name);
		}
		return this._icons[style][name].imageURL;
	},
	getShadow: function(style, name){
		if( !this.has(style,name) ){
			this.buildIcon(style, name);
		}
		return this._icons[style][name].shadow;
	}
};


Number.prototype.toRad = function() {
	return this * Math.PI / 180;
};

Number.prototype.toDeg = function() {
	return this * 180 / Math.PI;
};


/**
 *
 * @param p1 google.maps.LatLng
 * @param p2 google.maps.LatLng
 * @returns haversine distance
 */
function distHaversine(p1, p2) {
	var R = 6371000;

	var p1lat = p1.lat().toRad();
	var p1lng = p1.lng().toRad();

	var p2lat = p2.lat().toRad();
	var p2lng = p2.lng().toRad();

	var dLat  = p2lat - p1lat;
	var dLong = p2lng - p1lng;

	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	        Math.cos(p1lat) * Math.cos(p2lat) * Math.sin(dLong/2) * Math.sin(dLong/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c;
	return d;
}
/**
 *
 */
google.maps.LatLng.prototype.moveTowards = function(point, distance) {
    var lat1 = this.lat().toRad();
    var lon1 = this.lng().toRad();
    var lat2 = point.lat().toRad();
    var lon2 = point.lng().toRad();
    var dLon = (point.lng() - this.lng()).toRad();

    // Find the bearing from this point to the next.
    var brng = Math.atan2(Math.sin(dLon) * Math.cos(lat2),
                          Math.cos(lat1) * Math.sin(lat2) -
                          Math.sin(lat1) * Math.cos(lat2) *
                          Math.cos(dLon));

    var angDist = distance / 6371000;  // Earth's radius.

    // Calculate the destination point, given the source and bearing.
    lat2 = Math.asin(Math.sin(lat1) * Math.cos(angDist) +
                     Math.cos(lat1) * Math.sin(angDist) *
                     Math.cos(brng));

    lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(angDist) *
                             Math.cos(lat1),
                             Math.cos(angDist) - Math.sin(lat1) *
                             Math.sin(lat2));

    if (isNaN(lat2) || isNaN(lon2)) return null;

    return new google.maps.LatLng(lat2.toDeg(), lon2.toDeg());
};

/**
 *
 */
google.maps.Polyline.prototype.getLength = function() {
   var dist = 0;
   var path = this.getPath();
   for (var i = 1; i < path.getLength(); i++) {
      dist += distHaversine(path.getAt(i), path.getAt(i-1));
   }
   return dist;
};

/**
 *
 */
google.maps.Polyline.prototype.getCenter = function(){
	var len = this.getLength() / 2;
	var path = this.getPath();
	var from = path.getAt(0);
	for (var i = 1; i < path.getLength(); i++) {
		var to = path.getAt(i);
		var seg = distHaversine(from, to);
		if( len > seg ){
			len -= seg;
		} else {
			return from.moveTowards( to, len );
		}
		from = to;
	}
	return null;
};


google.maps.LatLngBounds.prototype.includes = function(bounds) {
	debug(this + " / " + bounds);

	var this_north = this.getNorthEast().lat();
	var this_south = this.getSouthWest().lat();

	var bnds_north = bounds.getNorthEast().lat();
	var bnds_south = bounds.getSouthWest().lat();

	return this_north > bnds_north && this_south < bnds_south;
};