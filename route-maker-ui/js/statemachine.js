var StateMachine = function(starts, ends, transitions) {
	this.starts = starts;
	this.ends = ends;
	this.transitions = transitions;
	this.current = null;
};

StateMachine.prototype = {
	is: function(state){
		return state === this.current;
	},
	next: function(next){
		if(this.current){
			var end = this.ends[this.current];
			if( end ){
				end();
			}
			if(this.transitions[this.current]){
				var transition = this.transitions[this.current][next];
				if( transition ){
					transition();
				}
			}
		}
		var start = this.starts[next];
		if( start ){
			start();
		}
		this.current = next;
	},
	tr: function(if_state, next){
		if(this.is(if_state)){
			this.next(next);
		}
	}
};