#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import Context, loader
from django.utils import simplejson as json
from googlemaps import GoogleMaps
import logging


logger = logging.getLogger('maps')


def index(request):
    t = loader.get_template('onlinemaps/index.html')
    c = Context({
        'response': "Hello, world. You're at the onlinemaps index.",
    })
    return HttpResponse(t.render(c))


def search(request):
    gmaps = GoogleMaps(api_key='AIzaSyCvsgM5i6-MrX0uGAS0qPpAdk0-BHf0AIA')
    
    address = '85 rue Henri Poincar�, 06410 Biot, France'
    destination = gmaps.latlng_to_address(43.581137, 7.124977)
    directions = gmaps.directions(address, destination)
    
    directions = json.dumps(directions, encoding='utf-8', indent=4)
    
    """
    print directions['Directions']['Duration']['seconds']
    for step in directions['Directions']['Routes'][0]['Steps']:
        print step['Point']['coordinates'][1], step['Point']['coordinates'][0]
        print step['descriptionHtml']
    """
    return render_to_response('onlinemaps/search.html',
                              {'directions': directions})

