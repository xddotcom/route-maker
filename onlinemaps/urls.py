'''
Created on 2012-6-29

@author: DXD.Spirits
'''

from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('onlinemaps.views',
    url(r'^$', 'index'),
    url(r'^search', 'search'),
)
