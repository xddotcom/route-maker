'''
Created on 2012-5-19

@author: DXD.Spirits
'''
from googlemaps import GoogleMaps

def search():
    gmaps = GoogleMaps(api_key='AIzaSyCvsgM5i6-MrX0uGAS0qPpAdk0-BHf0AIA')
    
    address = 'Constitution Ave NW & 10th St NW, Washington, DC'
    #lat, lng = gmaps.address_to_latlng(address)
    #print lat, lng
    #38.8921021 -77.0260358
    
    destination = gmaps.latlng_to_address(38.887563, -77.019929)
    #print destination
    
    #local = gmaps.local_search('cafe near ' + destination)
    #print local['responseData']['results'][0]['titleNoFormatting']
    
    directions = gmaps.directions(address, destination)
    '''
    print directions['Directions']['Distance']['meters']
    print directions['Directions']['Duration']['seconds']
    for step in directions['Directions']['Routes'][0]['Steps']:
        print step['Point']['coordinates'][1], step['Point']['coordinates'][0] 
        print step['descriptionHtml']
    '''
    return destination

if __name__ == '__main__':
    print(search())
